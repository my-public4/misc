wget -c https://services.gradle.org/distributions/gradle-8.2.1-bin.zip -P /tmp
ls /tmp
sudo unzip -d /opt/gradle /tmp/gradle-8.2.1-bin.zip

sudo vi /etc/profile.d/gradle.sh
# add 2 lines 
export GRADLE_HOME=/opt/gradle/gradle-8.2.1
export PATH=${GRADLE_HOME}/bin:${PATH}

sudo chmod +x /etc/profile.d/gradle.sh
source /etc/profile.d/gradle.sh